﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TowerDefence.Units;

namespace TowerDefenceTests
{
    [TestClass]
    public class TowerDefenceUnitTest
    {
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void CheckNegativeAttackSpeed()
        {
            Tower tower = new Tower(1,-1,1);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void CheckNegativeDamage()
        {
            Tower tower = new Tower(-1, 1, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void CheckNegativeDistance()
        {
            Tower tower = new Tower(1, 1, -1);
        }
    }
}
