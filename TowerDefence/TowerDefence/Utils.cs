namespace TowerDefence
{
	public delegate void AttackEventHandler(float Damage);
	public delegate void SlowdownEventHandler(float Slowdown);

    public enum BoardPieceType
    {
        StdTower1,
        StdTower2,
        StdTower3,
        SlowTower1,
        SlowTower2,
        SlowTower3,
        Stone,
        Grass,
        Road,
        Empty
    };
}