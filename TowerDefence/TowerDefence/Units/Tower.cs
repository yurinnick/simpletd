﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TowerDefence.UnitsInterfaces;
using Microsoft.Xna.Framework.Graphics;
using TowerDefence;

namespace TowerDefence.Units
{
    public class Tower: ITower
    {
        private const int _MAX_LEVEL = 3;
        private const float _DAMAGE_SCALE = 1.5F;
        private const float _ATTACK_SPEED_SCALE = 1.2F;
        private const float _DISTANCE_SCALE = 1.5F;

        private int BaseDamage;
        private float BaseAttackSpeed;
        private float BaseDistance;

        public Texture2D Sprite { get; set; }

        private float _damage;
        public float Damage {
            get
            {
                return _damage;
            }
            private set
            {
                if (value > 0)
                    _damage = value;
                else throw new FormatException("Damage can't be negative");
            }
        }

        private float _attackSpeed;
        public float AttackSpeed {
            get
            {
                return _attackSpeed;
            }
            private set
            {
                if (value > 0)
                    _attackSpeed = value;
                else throw new FormatException("Attack speed can't be negative");
            }
        }

        private float _distance;
        public float Distance {
            get 
            {
                return _distance;
            }
            private set
            {
                if (value > 0)
                    _distance = value;
                else throw new FormatException("Distance can't be negative");
            }
        }

        private int _level;
        public int Level {
            get
            {
                return _level;
            }
            set
            {
                if ((value >= 1) && (value <= _MAX_LEVEL))
                {
                    _level = value;
                    Damage = BaseDamage * Level * _DAMAGE_SCALE;
                    AttackSpeed = BaseAttackSpeed * Level * _ATTACK_SPEED_SCALE;
                    Distance = BaseDistance * Level * _DISTANCE_SCALE;
                }
                else throw new FormatException("Level can be only from 1 to " + _MAX_LEVEL.ToString());
            }
        }

        public Tower(int BaseDamage, float BaseAttackSpeed, float BaseDistance)
        {
            this.BaseDamage = BaseDamage;
            this.BaseAttackSpeed = BaseAttackSpeed;
            this.BaseDistance = BaseDistance;
            this.Level = 1;
        }

        public event AttackEventHandler OnAttack;

        public void Attack()
        {
            if (OnAttack != null)
            {
                OnAttack(this.Damage);
            }
        }

        public void Spawn() { }
        public void Update() { }
        public void Die() { }
    }
}
