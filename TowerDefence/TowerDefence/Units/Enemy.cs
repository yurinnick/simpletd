﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TowerDefence;

namespace TowerDefence.Units
{
    public class Enemy
    {
        private float Health { get; set; }
        private float MovementSpeed { get; set; }

        public Enemy(int health, int movementSpeed)
        {
            this.Health = health;
            this.MovementSpeed = movementSpeed;
        }

        public bool IsAlive
        {
            get
            {
                return (Health !=0);
            }
        }

        public void Attacked(float damage)
        {
            Health -= damage;
        }

        public void Slowed(float slowdown)
        {
            MovementSpeed *= slowdown;
        }
    }
}
