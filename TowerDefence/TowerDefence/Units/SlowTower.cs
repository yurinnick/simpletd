using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TowerDefence.UnitsInterfaces;
using Microsoft.Xna.Framework.Graphics;
using TowerDefence;

namespace TowerDefence.Units
{
	public class SlowTower: Tower
	{
		public event SlowdownEventHandler OnSlow;

        public void Slow()
        {
            if (OnSlow != 0)
            {
                OnSlow(this.Slowdown);
            }
        }
	}
}