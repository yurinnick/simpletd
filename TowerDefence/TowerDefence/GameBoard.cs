﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TowerDefence
{
    public class GameBoard
    {
        public const int _GAMEBOARD_WIDTH = 20;
        public const int _GAMEBOARD_HEIGHT = 10;

        private BoardPiece[,] gameBoard = new BoardPiece[_GAMEBOARD_WIDTH, _GAMEBOARD_HEIGHT];

        private List<Vector2> roadTracker = new List<Vector2>();

        public void Clear()
        {
            for (int i = 0; i < _GAMEBOARD_WIDTH; i++)
            {
                for (int j = 0; j < _GAMEBOARD_HEIGHT; j++)
                {
                    gameBoard[i, j].Type = BoardPieceType.Empty;
                }
            }
        }

        public void SetPieceType(BoardPieceType type, int x, int y)
        {
            gameBoard[x, y].Type = type;
        }
    }
}
