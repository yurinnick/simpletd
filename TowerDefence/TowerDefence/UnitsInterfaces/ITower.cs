﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace TowerDefence.UnitsInterfaces
{
    public interface ITower: IUnit
    {
        //Level-based tower parameters
        float Damage { get; }
        float AttackSpeed { get; }
        float Distance { get; }
        int Level { get; set; }

        event AttackEventHandler OnAttack;

        void Attack();
    }
}
