﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using TowerDefence;

namespace TowerDefence.UnitsInterfaces
{
    public interface IEnemy : IUnit
    {
        float Health { get; set; }
        float MovementSpeed { get; set; }
        int Level { get; set; }
        bool IsAlive { get; }

        void Attacked(float damage);
        void Slowed(float slowdown);
    }
}
