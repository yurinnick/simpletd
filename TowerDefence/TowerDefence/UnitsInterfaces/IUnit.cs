﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace TowerDefence.UnitsInterfaces
{
    public interface IUnit
    {
        Texture2D Sprite { get; set; }

        void Spawn();
        void Update();
        void Die();
    }
}
