﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace TowerDefence
{
    public class BoardPiece
    {
        public BoardPieceType Type;

        public Texture2D Texture;

        public BoardPiece(BoardPieceType type)
        {
            this.Type = type;
        }

        public void SetPieceType(BoardPieceType type)
        {
            this.Type = type;
        }

        private Rectangle GetSourceRect()
        {
            Rectangle rect = new Rectangle();

            return rect;
        }
    }
}
